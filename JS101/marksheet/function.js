

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function () {
      if (rawFile.readyState === 4 && rawFile.status == "200") {
        callback(rawFile.responseText);
      }
    };
    rawFile.send(null);
  }


function generateSheet(){

    readTextFile('marks.json', function(data){



        let json = JSON.parse(data);
        console.log(json);

        let body = document.getElementById("list");

        var total = 0;
        var outoftotal = 0;

        for(var i = 0; i < json.length; i++){

            console.log(json[i])
            let subject = json[i]['subject'];
            let value = json[i]['value'];
            let outof = json[i]['outof'];


            var row = document.createElement("tr");

            var subjectCell = document.createElement("td");
            var ValueCell = document.createElement("td");
            var OutOfCell = document.createElement("td");


            subjectCell.innerHTML = subject;
            ValueCell.innerHTML = value;
            OutOfCell.innerHTML = outof;


            // add value to previous total value
            total = total + value;
            outoftotal = outoftotal + outof;

            row.appendChild(subjectCell);
            row.appendChild(ValueCell);
            row.appendChild(OutOfCell);

            body.appendChild(row);


        }



        // here we add one more row just for total 
        var row = document.createElement("tr");
        var totalCell = document.createElement("td");
        var valueCell = document.createElement("td");
        var outofCell = document.createElement("td");

        totalCell.innerHTML = 'TOTAL';
        valueCell.innerHTML = total;
        outofCell.innerHTML = outoftotal;
        

        row.appendChild(totalCell);
        row.appendChild(valueCell);
        row.appendChild(outofCell);

        body.appendChild(row);


        // Average of marks
        var row = document.createElement("tr");
        var totalCell = document.createElement("td");
        var valueCell = document.createElement("td");
        var outofCell = document.createElement("td");

        totalCell.innerHTML = 'Average';
        valueCell.innerHTML = total / json.length;
        outofCell.innerHTML = outoftotal / json.length;
        

        row.appendChild(totalCell);
        row.appendChild(valueCell);
        row.appendChild(outofCell);

        body.appendChild(row);

        // Percentage of marks
        var row = document.createElement("tr");
        var totalCell = document.createElement("td");
        var valueCell = document.createElement("td");
        var outofCell = document.createElement("td");

        totalCell.innerHTML = 'Percentage';
        valueCell.innerHTML = parseFloat(  ( total * 100 ) / outoftotal ).toFixed(2) + '%';  
        outofCell.innerHTML =  '';
        

        row.appendChild(totalCell);
        row.appendChild(valueCell);
        row.appendChild(outofCell);

        body.appendChild(row);





    })

}