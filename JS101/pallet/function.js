
// Canvas 
// https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Pixel_manipulation_with_canvas


let canvas = document.getElementById('canvas');
let context = canvas.getContext('2d');

var newImg = new Image;
newImg.onload = function() {
    // let img = this.src;
    context.drawImage(newImg, 0, 0, newImg.width,    newImg.height, 0, 0, canvas.width, canvas.height); 
}
newImg.src = 'pallet.jpeg';

canvas.addEventListener('mousemove', handleClick, true);

function getPosition(obj) {
    var curleft = 0, curtop = 0;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return { x: curleft, y: curtop };
    }
    return undefined;
}


function getColorIndicesForCoord(x, y, width) {
    var red = y * (width * 4) + x * 4;
    return [red, red + 1, red + 2, red + 3];
}


function rgbToHex(r, g, b) {

    if (r > 255 || g > 255 || b > 255)
        throw "Invalid color component";
    return ((r << 16) | (g << 8) | b).toString(16);
}

function handleClick(e) {

    let pos = getPosition(this);

    
    var x = e.pageX - pos.x;
    var y = e.pageY - pos.y;
    var coord = "x=" + x + ", y=" + y;
    let color = getColorIndicesForCoord(x, y, canvas.width);
    console.log(color);


    let p = canvas.getContext('2d').getImageData(x, y, 1, 1).data;
    console.log(p);
    var hex = "#" + ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6);
    console.log(hex);

    document.getElementById('picker').style.background = hex;


}

























































































