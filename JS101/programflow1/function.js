function readTextFile(file, callback) {
  var rawFile = new XMLHttpRequest();
  rawFile.overrideMimeType("application/json");
  rawFile.open("GET", file, true);
  rawFile.onreadystatechange = function () {
    if (rawFile.readyState === 4 && rawFile.status == "200") {
      callback(rawFile.responseText);
    }
  };
  rawFile.send(null);
}

//usage:

// step 1 is to fetch data from json and print it out
function getJsonData() {
  readTextFile("students.json", function (text) {
    var data = JSON.parse(text);
    console.log(data);

    // now the next step is to intially loop to create table rows
    let body = document.getElementById("list");

    for (var j = 0; j < data.length; j++) {
      // creates a table row
      var row = document.createElement("tr");
      // Create a <td> element and a text node, make the text
      // node the contents of the <td>, and put the <td> at
      // the end of the table row
      var cell1 = document.createElement("td");
      var cell2 = document.createElement("td");
      var cell3 = document.createElement("td");

      var idText = document.createTextNode(data[j]["_id"]);
      cell1.appendChild(idText);

      var nameText = document.createTextNode(data[j]["name"]);
      cell2.appendChild(nameText);

      let body2 = document.getElementById("list2");
      let btn = document.createElement("button");
      btn.data = data[j];
      btn.data;
      btn.onclick = function () {
        console.log(this.data);

        for (var p = 0; p < this.data.scores.length; p++) {
          var row = document.createElement("tr");
          // Create a <td> element and a text node, make the text
          // node the contents of the <td>, and put the <td> at
          // the end of the table row
          var cell1 = document.createElement("td");
          var cell2 = document.createElement("td");

          var idText = document.createTextNode(this.data.scores[p]["score"]);
          cell1.appendChild(idText);

          var nameText = document.createTextNode(this.data.scores[p]["type"]);
          cell2.appendChild(nameText);

          row.appendChild(cell1);
          row.appendChild(cell2);

          document.body2.appendChild(row);
        }
      };

      // function(){
      //     console.log(data[j]['scores'])
      // }

      row.appendChild(cell1);
      row.appendChild(cell2);
      row.appendChild(btn);

      body.appendChild(row);
    }

    // add the row to the end of the table body
  });
}
