


$(document).ready(function () {

    reloadTable();

    $('input[name=search]').on('keyup', function (e) {
        e.preventDefault();

        let search = $(this).val();
        reloadTable(search);


    })

    function reloadTable(search = '') {

        $.post(
            "ajax.php",
            { 'type': 'list', data: search },
            function (data, status) {
                console.log(data);

                let json = JSON.parse(data);

                // // empty the table body and reload all records
                let body = document.getElementById("list");

                $('#list').empty();

                for (var i = 0; i < json.length; i++) {

                    var data1 = json[i];
                    var row = document.createElement("tr");

                    var Cell0 = document.createElement("td");
                    var Cell1 = document.createElement("td");
                    var Cell2 = document.createElement("td");
                    var Cell3 = document.createElement("td");
                    var Cell4 = document.createElement("td");

                    Cell0.innerHTML = data1["id"] + '<input type="hidden" name="id" value="' + data1["id"] + '">';
                    Cell1.innerHTML = '<span>' + data1["name"] + '</span>' + '<input type="hidden" name="name" value="' + data1["name"] + '">';
                    Cell2.innerHTML = '<span>' + data1["email"] + '</span>' + '<input type="hidden" name="email" value="' + data1["email"] + '">';
                    Cell3.innerHTML = '<span>' + data1["percentage"] + '</span>' + '<input type="hidden" name="percentage" value="' + data1["percentage"] + '">';
                    Cell4.innerHTML = '<button type="button" id="' + data1['id'] + '" class="btn btn-danger">DELETE</button> <button type="button" id="' + data1['id'] + '" class="btn btn-warning">EDIT</button> <button type="button" id="' + data1['id'] + '" class="btn btn-success" style="display:none">SAVE</button>'

                    row.appendChild(Cell0);
                    row.appendChild(Cell1);
                    row.appendChild(Cell2);
                    row.appendChild(Cell3);
                    row.appendChild(Cell4)

                    body.appendChild(row);
                }

                $('.btn-danger').on('click', function (e) {
                    e.preventDefault();
                    console.log(this.id);

                    $.post(
                        "ajax.php",
                        { 'type': 'delete', data: this.id },
                        function (data, status) {
                            reloadTable();
                        }
                    );

                })

                $('.btn-warning').on('click', function (e) {
                    e.preventDefault();
                    console.log(this.id);

                    $(this).closest('tr').find('td:nth-child(2)').find('input').attr('type', 'text');
                    $(this).closest('tr').find('td:nth-child(2)').find('span').hide();

                    $(this).closest('tr').find('td:nth-child(3)').find('input').attr('type', 'text');
                    $(this).closest('tr').find('td:nth-child(3)').find('span').hide();

                    $(this).closest('tr').find('td:nth-child(4)').find('input').attr('type', 'text');
                    $(this).closest('tr').find('td:nth-child(4)').find('span').hide();

                    $(this).closest('tr').find('td:nth-child(5)').find('button:nth-child(1)').hide()
                    $(this).closest('tr').find('td:nth-child(5)').find('button:nth-child(2)').hide()
                    $(this).closest('tr').find('td:nth-child(5)').find('button:nth-child(3)').show()


                })



                $('.btn-success').on('click', function (e) {
                    e.preventDefault();
                    console.log(this.id);

                    var id = $(this).closest('tr').find('td:nth-child(1)').find('input').val();
                    var name = $(this).closest('tr').find('td:nth-child(2)').find('input').val();
                    var email = $(this).closest('tr').find('td:nth-child(3)').find('input').val();
                    var percentage = $(this).closest('tr').find('td:nth-child(4)').find('input').val();

                    var obj = { id, name, email, percentage }
                    console.log(obj);

                    $.post(
                        "ajax.php",
                        { 'type': 'edit', data: obj },
                        function (data, status) {
                            reloadTable();
                        }
                    );






                })


            }
        );






    }





    $("#add-form").on("submit", function (event) {
        event.preventDefault();
        console.log($(this).serialize());
        var data = $(this).serializeArray();


        // submit form for ajax

        $.post(
            "ajax.php",
            { 'type': 'add', data: data },
            function (data, status) {
                reloadTable();
            }
        );

    });












});