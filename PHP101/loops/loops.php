<?php

    $name = "Ali";

    // how arrays are defined and how they work 
    $cars = array("Volvo", "BMW", "Toyota");
    $automobiles = ["car", "bus", "motorcycle", "jeep"]; 
    // indexing starts with 0 onwards 

    // count 
    // echo count($automobiles);

    // while

    // print the array line by line
    // what is the largest index number the array $automobiles has ?
    // largest index is 3

    // $index = 0;

    // while($index <= 3){
    //     echo 'The value is: ' . $automobiles[$index] . ' <br>';
    //     $index++;
    // }





    // $x = 1;

    // while($x <= 100) {
    //     echo "The number is: $x <br>";
    //     $x = $x + 5;
    // }

    // do .. while

    // $r = 0;
    // while ($r <= 56){
    //     echo "Hello R !! <br>";
    //     $r = 73;
    // };

    // $x = 0;
    // do {
    //     // ;
    //     echo "Hello !! <br>";
    //     $x = 73;

    // } while ($x <= 56);

    // for

    // for($i = 0; $i < 50; $i = $i + 5){
    //     echo "IS " . $i . '<br>';
    // }

    // print 2 ka table in for loop
    
    // for($i = 1; $i <= 10; $i = $i + 1){
    //     echo  "2 x $i = " . $i * 2 . "<br>";
    // }

    // foreach

    foreach($automobiles as $vehicle){
        echo $vehicle . "<br>";
    }






    















?>