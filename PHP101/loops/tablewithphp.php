<!DOCTYPE html>
<html>






<?php

/*

    use case is ... you will have a students marksheet 
    // subjexts you will choose ... 
    // print that markssheet in html with the help of php loops 
    // use bootstrap for good visual of tables 
    // make calculations as well 
    such as percentage, total ... gross total and any more information 
    like GPA etc 

    hint 

    array of arrays will be used just like today

*/



?>

















<head>
    <title>Tables</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
</head>

<body>


<?php

    $R = [
        [
            "first_name" => "ALI",
            "last_name" => "KHHAN",
            "handle" => "@alikhan1"
        ],
        [
            "first_name" => "Usama",
            "last_name" => "KHHAN",
            "handle" => "@usamakhan"
        ],
        [
            "first_name" => "Noor",
            "last_name" => "mehel",
            "handle" => "@sidraanjum"
        ],
        [
            "first_name" => "gool",
            "last_name" => "gappa",
            "handle" => "@goolgappa"
        ]
    ];


?>









<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <?php 
        $x = 1;
        foreach($R as $r){

    ?>
        <tr>
            <th scope="row"> <?php echo $x; ?> </th>
            <td> <?php echo $r['first_name']; ?>  </td>
            <td> <?php echo $r['last_name']; ?> </td>
            <td> <?php echo $r['handle']; ?> </td>
        </tr>
    <?php
        $x++;
        }
    ?>
    
  </tbody>
</table>

</body>

</html>